import { createContext, useReducer, useEffect } from 'react';
import axios from 'axios';
import { authReducer } from 'src/reducers/authReducer';
import { apiUrl, LOCAL_STORAGE_TOKEN_NAME } from './constants';
import setAuthToken from 'src/untils/setAuthToken';

export const AuthContext = createContext();
const AuthContextProvider = ({ children }) => {
	const [authState, dispatch] = useReducer(authReducer, {
		authLoading: true,
		isAuthenticated: false,
		user: null,
	});

	// Authenticated user
	const loadUser = async () => {
		const token = localStorage[LOCAL_STORAGE_TOKEN_NAME];
		if (token) {
			setAuthToken(token);
		}

		try {
			const response = await axios.get(`${apiUrl}/auth`);
			if (response.data.success) {
				dispatch({
					type: 'SET_AUTH',
					payload: {
						isAuthenticated: true,
						user: response.data.user,
					},
				});
			}
		} catch (error) {
			localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME);
			setAuthToken(null);
			dispatch({
				type: 'SET_AUTH',
				payload: {
					isAuthenticated: false,
					user: null,
				},
			});
		}
	};

	useEffect(() => {
		loadUser();
	}, []);

	// Login
	const loginUser = async (userForm) => {
		try {
			const respone = await axios.post(`${apiUrl}/auth/login`, userForm);
			if (respone.data.success) {
				localStorage.setItem(
					LOCAL_STORAGE_TOKEN_NAME,
					respone.data.accessToken
				);
			}

			await loadUser();
			return respone.data;
		} catch (error) {
			if (error.response.data) {
				return error.response.data;
			}
			return { success: false, message: error.message };
		}
	};

	// reigsterUser
	const registerUser = async (userForm) => {
		try {
			const respone = await axios.post(
				`${apiUrl}/auth/register`,
				userForm
			);
			if (respone.data.success) {
				localStorage.setItem(
					LOCAL_STORAGE_TOKEN_NAME,
					respone.data.accessToken
				);
			}

			await loadUser();
			return respone.data;
		} catch (error) {
			if (error.response.data) {
				return error.response.data;
			}
			return { success: false, message: error.message };
		}
	};

	// Logout
	const logoutUser = () => {
		localStorage.removeItem(LOCAL_STORAGE_TOKEN_NAME);
		dispatch({
			type: 'SET_AUTH',
			payload: { isAuthenticated: false, user: null },
		});
	};

	const authContextData = { loginUser, registerUser, logoutUser, authState };

	return (
		<AuthContext.Provider value={authContextData}>
			{children}
		</AuthContext.Provider>
	);
};

export default AuthContextProvider;
