import React from 'react';
import LoginForm from 'src/components/auth/LoginForm';
import RegisterForm from 'src/components/auth/RegisterForm';
import { AuthContext } from 'src/contexts/AuthContext';
import { useContext } from 'react';
import { Redirect } from 'react-router-dom';
import Spinner from 'react-bootstrap/Spinner';

const Auth = ({ authRoute }) => {
	const {
		authState: { authLoading, isAuthenticated },
	} = useContext(AuthContext);

	if (isAuthenticated) return <Redirect to="/dashboard" />;

	const Loading = (
		<div className="d-flex justify-content-center mt-2">
			<Spinner animation="border" variant="info" />
		</div>
	);

	const Form = (
		<>{authRoute === 'login' ? <LoginForm /> : <RegisterForm />}</>
	);

	const body = authLoading ? Loading : Form;

	return (
		<div className="landing">
			<div className="dark-overlay">
				<div className="landing-inner">
					<h1>LearnIt</h1>
					<h4>Keep track of what you are learning</h4>
					{body}
				</div>
			</div>
		</div>
	);
};

export default Auth;
