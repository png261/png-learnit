import { Route, Redirect } from 'react-router-dom';
import { useContext } from 'react';
import { AuthContext } from 'src/contexts/AuthContext';
import Spinner from 'react-bootstrap/Spinner';
import NavbarMenu from 'src/components/layout/NavbarMenu';

const AuthenticatedGuard = ({ children }) => {
	const {
		authState: { authLoading, isAuthenticated },
	} = useContext(AuthContext);

	if (authLoading) {
		return (
			<div className="spinner-container">
				<Spinner animation="border" variant="info" />
			</div>
		);
	}

	if (!isAuthenticated) {
		return <Redirect to="/login" />;
	}

	return (
		<>
			<NavbarMenu />
			{children}
		</>
	);
};
export default AuthenticatedGuard;
