import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Landing from 'src/components/layout/Landing';
import Auth from 'src/views/Auth';
import Dashboard from 'src/views/Dashboard';
import AuthContextProvider from 'src/contexts/AuthContext';
import PostContextProvider from 'src/contexts/PostContext';
import AuthenticatedGuard from 'src/guards/AuthenticatedGuard';

function App() {
	return (
		<AuthContextProvider>
			<PostContextProvider>
				<Router>
					<Switch>
						<Route exact path="/" component={Landing} />
						<Route
							exact
							path="/login"
							render={(props) => (
								<Auth {...props} authRoute="login" />
							)}
						/>
						<Route
							exact
							path="/register"
							render={(props) => (
								<Auth {...props} authRoute="register" />
							)}
						/>
						<AuthenticatedGuard>
							<Route
								exact
								path="/dashboard"
								component={Dashboard}
							/>
						</AuthenticatedGuard>
					</Switch>
				</Router>
			</PostContextProvider>
		</AuthContextProvider>
	);
}

export default App;
