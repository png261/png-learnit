import Button from 'react-bootstrap/Button';
import playIcon from 'src/assets/play-btn.svg';
import editIcon from 'src/assets/pencil.svg';
import deleteIcon from 'src/assets/trash.svg';
import { PostContext } from 'src/contexts/PostContext';
import { useContext } from 'react';

const ActionButtons = ({ url, _id }) => {
	const { deletePost, findPost, setShowToast, setShowUpdatePostModal } =
		useContext(PostContext);
	const removePost = async (id) => {
		const { success, message } = await deletePost(id);
		setShowToast({
			show: true,
			message,
			type: success ? 'success' : 'danger',
		});
	};

	const choosePost = (postId) => {
		findPost(postId);
		setShowUpdatePostModal(true);
	};

	return (
		<>
			<Button className="post-button" href={url} target="_blank">
				<img src={playIcon} alt="play" width="32" height="32" />
			</Button>
			<Button
				className="post-button"
				onClick={choosePost.bind(this, _id)}
			>
				<img src={editIcon} alt="edit" width="24" height="24" />
			</Button>
			<Button className="post-button" onClick={() => removePost(_id)}>
				<img src={deleteIcon} alt="delete" width="24" height="24" />
			</Button>
		</>
	);
};

export default ActionButtons;
