import React, { useContext, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { Link, useHistory } from 'react-router-dom';
import { AuthContext } from 'src/contexts/AuthContext';
import AlertMessage from 'src/components/layout/AlertMessage';

const LoginForm = () => {
	// Context
	const { loginUser } = useContext(AuthContext);

	// Router
	const history = useHistory();

	// Local state
	const [loginForm, setLoginForm] = useState({
		username: '',
		password: '',
	});

	const onChangeLoginForm = (e) => {
		setLoginForm({ ...loginForm, [e.target.name]: e.target.value });
	};

	const [alert, setAlert] = useState(null);

	const { username, password } = loginForm;

	const login = async (e) => {
		try {
			e.preventDefault();
			const loginData = await loginUser(loginForm);

			if (loginData.success) {
				return history.push('/dashboard');
			}
			setAlert({ type: 'danger', message: loginData.message });
			setTimeout(() => setAlert(null), 2000);
		} catch (error) {
			console.log(error);
		}
	};

	return (
		<>
			<Form className="my-4" onSubmit={login}>
				<AlertMessage info={alert} />
				<Form.Group>
					<Form.Control
						type="text"
						placeholder="Username"
						name="username"
						required
						vale={username}
						onChange={onChangeLoginForm}
					/>
				</Form.Group>
				<Form.Group>
					<Form.Control
						type="password"
						placeholder="Password"
						name="password"
						required
						vale={password}
						onChange={onChangeLoginForm}
					/>
				</Form.Group>
				<Button variant="success" type="submit">
					Login
				</Button>
			</Form>
			<p>
				Don't have an account?
				<Link to="/register">
					<Button variant="info" size="sm" className="ml-2">
						Register
					</Button>
				</Link>
			</p>
		</>
	);
};

export default LoginForm;
