import Alert from 'react-bootstrap/Alert';

const AlertMessage = ({ info }) => (
	<>{info && <Alert variant={info.type}>{info.message}</Alert>}</>
);

export default AlertMessage;
