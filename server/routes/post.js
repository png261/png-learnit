const express = require('express');
const router = express.Router();
const verifyToken = require('../middleware/auth');

const Post = require('../models/Post');

//@route GET api/posts
//@desc Get posts
//@access Private
router.get('/', verifyToken, async (req, res) => {
	try {
		const posts = await Post.find({ user: req.userId }).populate('user', [
			'username',
		]);
		res.json({ success: true, posts });
	} catch (error) {
		console.log(error);
		res.status(500).json({
			success: false,
			message: 'Internal server error',
		});
	}
});

//@route GET api/post
//@desc Get post
//@access Private
router.get('/:id', verifyToken, async (req, res) => {
	try {
		const getCondition = {
			_id: req.params.id,
			user: req.userId,
		};

		const post = await Post.findOne(getCondition).populate('user', [
			'username',
		]);

		if (!post) {
			return res.status(404).json({
				success: false,
				message: 'Post id not found',
			});
		}

		res.json({ success: true, post });
	} catch (error) {
		console.log(error);
		res.status(500).json({
			success: false,
			message: 'Internal server error',
		});
	}
});

//@route PATH api/posts
//@desc Update posts
//@access Private
router.patch('/:id', verifyToken, async (req, res) => {
	// Simple validation
	if (!req.body.title) {
		return res
			.status(400)
			.json({ success: false, message: 'Title is required' });
	}

	try {
		const updateCondition = { _id: req.params.id, user: req.userId };

		// add https to url
		if (req.body.url) {
			req.body.url = req.body.url.startsWith('https://')
				? req.body.url
				: 'https://' + req.body.url;
		}

		const updatedPost = await Post.findOneAndUpdate(
			updateCondition,
			req.body,
			{
				new: true,
			}
		);

		// User not authorised to update post or post not found
		if (!updatedPost) {
			return res.status(401).json({
				success: false,
				message: 'Post not found or user not authorised',
			});
		}

		res.json({
			success: true,
			message: 'Excellent progress!',
			post: updatedPost,
		});
	} catch (error) {
		console.log(error);
		res.status(500).json({
			success: false,
			message: 'Internal server error',
		});
	}
});

//@route POST api/posts
//@desc Create post
//@access Private
router.post('/', verifyToken, async (req, res) => {
	const { title, description, url, status } = req.body;

	if (!title) {
		return res
			.status(400)
			.json({ success: false, message: 'Title is required' });
	}

	try {
		const newPost = new Post({
			title,
			description,
			url: url && url.startsWith('https://') ? url : `https://${url}`,
			status: status,
			user: req.userId,
		});
		await newPost.save();
		res.json({ success: true, message: 'Happy learning!', post: newPost });
	} catch (error) {
		console.log(error);
		res.status(500).json({
			success: false,
			message: 'Internal server error',
		});
	}
});

// @route DELETE api/posts
// @desc Delete post
// @access Private
router.delete('/:id', verifyToken, async (req, res) => {
	try {
		const deleteCondition = { _id: req.params.id, user: req.userId };
		const deletedPost = await Post.findOneAndDelete(deleteCondition);

		// User not authorised or post not found
		if (!deletedPost) {
			return res.status(401).json({
				success: false,
				message: 'Post not found or user not authorised',
			});
		}
		res.json({
			success: true,
			message: `Post ${req.params.id} has been deleted`,
			post: deletedPost,
		});
	} catch (error) {
		console.log(error);
		res.status(500).json({
			success: false,
			message: 'Internal server error',
		});
	}
});

module.exports = router;
