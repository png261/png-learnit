const express = require('express');
const connectDB = require('./config/db');
const dotenv = require('dotenv');
const authRouter = require('./routes/auth');
const postRouter = require('./routes/post');
const cors = require('cors');

// Load config
dotenv.config({ path: './config/config.env' });

//connect mongoDb
connectDB();

const app = express();
// To get body data
app.use(express.json());

// User cors
app.use(cors());

// use routes
app.use('/api/auth', authRouter);
app.use('/api/posts', postRouter);

// Catch 404
app.use((req, res, next) => {
	res.status(404).json({ message: 'Url not found' });
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, console.log(`Server started on port ${PORT}`));
